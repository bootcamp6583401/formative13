package formative.thirteen.task1.repositories;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import formative.thirteen.task1.models.District;

public interface DistrictRepository extends CrudRepository<District, Integer> {
    List<District> findByCityId(int city_id);

}