package formative.thirteen.task1.repositories;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import formative.thirteen.task1.models.City;

public interface CityRepository extends CrudRepository<City, Integer> {
    List<City> findByProvinceId(int province_id);

}