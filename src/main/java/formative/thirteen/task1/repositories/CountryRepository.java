package formative.thirteen.task1.repositories;

import org.springframework.data.repository.CrudRepository;

import formative.thirteen.task1.models.Country;

public interface CountryRepository extends CrudRepository<Country, Integer> {

}