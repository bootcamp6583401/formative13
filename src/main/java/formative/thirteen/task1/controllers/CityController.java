package formative.thirteen.task1.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
// import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import formative.thirteen.task1.models.City;
import formative.thirteen.task1.repositories.CityRepository;

import java.util.Optional;

@Controller
@RequestMapping(path = "/cities")
public class CityController {
    @Autowired
    private CityRepository cityRepo;

    @PostMapping
    public @ResponseBody String addNewCity(@RequestBody City gym) {

        City n = new City();
        n.setName(gym.getName());
        cityRepo.save(n);
        return "Saved";
    }

    @GetMapping
    public @ResponseBody Iterable<City> getAllCitys() {
        return cityRepo.findAll();
    }

    @DeleteMapping(path = "/{id}")
    public ResponseEntity<City> deleteCity(@PathVariable int id) {
        Optional<City> optionalCity = cityRepo.findById(id);
        if (optionalCity.isPresent()) {
            City foundCity = optionalCity.get();
            cityRepo.delete(foundCity);
            return new ResponseEntity<>(foundCity, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

    }

    @GetMapping("/{id}")
    public ResponseEntity<City> getCityById(@PathVariable("id") int id) {
        Optional<City> userData = cityRepo.findById(id);

        if (userData.isPresent()) {
            return new ResponseEntity<>(userData.get(), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @PutMapping("/{id}")
    public ResponseEntity<City> updateCity(@PathVariable("id") int id, @RequestBody City gym) {
        Optional<City> gymData = cityRepo.findById(id);

        if (gymData.isPresent()) {
            City _gym = gymData.get();
            _gym.setName(gym.getName());
            return new ResponseEntity<>(cityRepo.save(_gym), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    // @PutMapping("/{id}/province/{provinceId}")
    // public ResponseEntity<City> setCity(@PathVariable("id") int id,
    // @PathVariable("provinceId") int provinceId) {
    // Optional<City> cityData = cityRepo.findById(id);
    // Optional<Province> region = regionRepo.findById(provinceId);

    // if (cityData.isPresent() && region.isPresent()) {
    // City _city = cityData.get();
    // Province _region = region.get();

    // _city.setRegion(_region);

    // return new ResponseEntity<>(cityRepo.save(_city), HttpStatus.OK);
    // } else {
    // return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    // }
    // }

}