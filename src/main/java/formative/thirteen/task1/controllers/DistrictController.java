package formative.thirteen.task1.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import formative.thirteen.task1.models.District;
import formative.thirteen.task1.repositories.DistrictRepository;

import java.util.Optional;

@Controller
@RequestMapping(path = "/districts")
public class DistrictController {
    @Autowired
    private DistrictRepository gymRepo;

    @PostMapping
    public @ResponseBody String addNewGym(@RequestBody District gym) {
        District n = new District();
        n.setName(gym.getName());
        gymRepo.save(n);
        return "Saved";
    }

    @GetMapping
    public @ResponseBody Iterable<District> getAllGyms() {
        return gymRepo.findAll();
    }

    @DeleteMapping(path = "/{id}")
    public ResponseEntity<District> deleteGym(@PathVariable int id) {
        Optional<District> optionalGym = gymRepo.findById(id);
        if (optionalGym.isPresent()) {
            District foundGym = optionalGym.get();
            gymRepo.delete(foundGym);
            return new ResponseEntity<>(foundGym, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

    }

    @GetMapping("/{id}")
    public ResponseEntity<District> getGymById(@PathVariable("id") int id) {
        Optional<District> userData = gymRepo.findById(id);

        if (userData.isPresent()) {
            return new ResponseEntity<>(userData.get(), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @PutMapping("/{id}")
    public ResponseEntity<District> updateGym(@PathVariable("id") int id, @RequestBody District gym) {
        Optional<District> gymData = gymRepo.findById(id);

        if (gymData.isPresent()) {
            District _gym = gymData.get();
            _gym.setName(gym.getName());
            return new ResponseEntity<>(gymRepo.save(_gym), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    // @PutMapping("/{id}/city/{cityId}")
    // public ResponseEntity<District> setCity(@PathVariable("id") int id,
    // @PathVariable("cityId") int cityId) {
    // Optional<District> gymData = gymRepo.findById(id);
    // Optional<City> city = cityRepo.findById(cityId);

    // if (gymData.isPresent() && city.isPresent()) {
    // District _gym = gymData.get();
    // City _city = city.get();

    // _gym.setCity(_city);

    // return new ResponseEntity<>(gymRepo.save(_gym), HttpStatus.OK);
    // } else {
    // return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    // }
    // }

}