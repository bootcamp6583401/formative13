package formative.thirteen.task1.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
// import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import formative.thirteen.task1.models.Province;
import formative.thirteen.task1.repositories.ProvinceRepository;

import java.util.Optional;

@Controller
@RequestMapping(path = "/provinces")
public class ProvinceController {
    @Autowired
    private ProvinceRepository regionRepo;

    @PostMapping
    public @ResponseBody String addNewRegion(@RequestBody Province region) {

        Province n = new Province();
        n.setName(region.getName());
        regionRepo.save(n);
        return "Saved";
    }

    @GetMapping
    public @ResponseBody Iterable<Province> getAllRegions() {
        return regionRepo.findAll();
    }

    @DeleteMapping(path = "/{id}")
    public ResponseEntity<Province> deleteRegion(@PathVariable int id) {
        Optional<Province> optionalRegion = regionRepo.findById(id);
        if (optionalRegion.isPresent()) {
            Province foundRegion = optionalRegion.get();
            regionRepo.delete(foundRegion);
            return new ResponseEntity<>(foundRegion, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

    }

    @GetMapping("/{id}")
    public ResponseEntity<Province> getRegionById(@PathVariable("id") int id) {
        Optional<Province> userData = regionRepo.findById(id);

        if (userData.isPresent()) {
            return new ResponseEntity<>(userData.get(), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @PutMapping("/{id}")
    public ResponseEntity<Province> updateRegion(@PathVariable("id") int id, @RequestBody Province region) {
        Optional<Province> regionData = regionRepo.findById(id);

        if (regionData.isPresent()) {
            Province _region = regionData.get();
            _region.setName(region.getName());
            return new ResponseEntity<>(regionRepo.save(_region), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    // @PutMapping("/{id}/country/{countryId}")
    // public ResponseEntity<Province> setWorld(@PathVariable("id") int id,
    // @PathVariable("countryId") int countryId) {
    // Optional<Province> regionData = regionRepo.findById(id);
    // Optional<Country> world = worldRepo.findById(countryId);

    // if (regionData.isPresent() && world.isPresent()) {
    // Province _region = regionData.get();
    // Country _world = world.get();

    // _region.setCountry(_world);

    // return new ResponseEntity<>(regionRepo.save(_region), HttpStatus.OK);
    // } else {
    // return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    // }
    // }

}