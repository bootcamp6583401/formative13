package formative.thirteen.task1.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
// import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import formative.thirteen.task1.models.Country;
import formative.thirteen.task1.repositories.CountryRepository;

import java.util.Optional;

@Controller
@RequestMapping(path = "/countries")
public class CountryController {
    @Autowired
    private CountryRepository worldRepo;

    @PostMapping
    public @ResponseBody String addNewWorld(@RequestBody Country world) {

        Country n = new Country();
        n.setName(world.getName());
        worldRepo.save(n);
        return "Saved";
    }

    @GetMapping
    public @ResponseBody Iterable<Country> getAllWorlds() {
        return worldRepo.findAll();
    }

    @DeleteMapping(path = "/{id}")
    public ResponseEntity<Country> deleteWorld(@PathVariable int id) {
        Optional<Country> optionalWorld = worldRepo.findById(id);
        if (optionalWorld.isPresent()) {
            Country foundWorld = optionalWorld.get();
            worldRepo.delete(foundWorld);
            return new ResponseEntity<>(foundWorld, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

    }

    @GetMapping("/{id}")
    public ResponseEntity<Country> getWorldById(@PathVariable("id") int id) {
        Optional<Country> userData = worldRepo.findById(id);

        if (userData.isPresent()) {
            return new ResponseEntity<>(userData.get(), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @PutMapping("/{id}")
    public ResponseEntity<Country> updateWorld(@PathVariable("id") int id, @RequestBody Country world) {
        Optional<Country> worldData = worldRepo.findById(id);

        if (worldData.isPresent()) {
            Country _world = worldData.get();
            _world.setName(world.getName());
            return new ResponseEntity<>(worldRepo.save(_world), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

}