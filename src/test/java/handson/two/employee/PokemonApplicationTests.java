package handson.two.employee;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import formative.thirteen.task1.PokemonApplication;

@SpringBootTest(classes = PokemonApplication.class)
class PokemonApplicationTests {

	@Test
	void contextLoads() {
	}

	@Test
	void oneEqualsOne() {
		assertEquals(getClass(), getClass());
	}

}

// package handson.two.employee;

// import static org.junit.jupiter.api.Assertions.assertEquals;

// import org.junit.jupiter.api.Test;
// import org.springframework.beans.factory.annotation.Autowired;
// import
// org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient;
// import org.springframework.boot.test.context.SpringBootTest;
// import org.springframework.http.MediaType;
// import org.springframework.test.web.reactive.server.WebTestClient;

// import handson.thirteen.task1.PokemonApplication;
// import handson.thirteen.task1.models.Pokemon;

// @SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT,
// classes = PokemonApplication.class)
// @AutoConfigureWebTestClient
// class PokemonApplicationTests {

// @Autowired
// private WebTestClient webTestClient;

// @Test
// void contextLoads() {
// }

// @Test
// void oneEqualsOne() {
// assertEquals(1, 1);
// }

// @Test
// void getAllPokemons() {
// webTestClient.get()
// .uri("/pokemons/")
// .accept(MediaType.APPLICATION_JSON)
// .exchange()
// .expectStatus().isOk()
// .expectHeader().contentType(MediaType.APPLICATION_JSON)
// .expectBodyList(Pokemon.class);
// }

// @Test
// void getPokemonById() {
// webTestClient.get()
// .uri("/pokemons/{id}", 1)
// .accept(MediaType.APPLICATION_JSON)
// .exchange()
// .expectStatus().isOk()
// .expectHeader().contentType(MediaType.APPLICATION_JSON)
// .expectBody(Pokemon.class);
// }

// }
